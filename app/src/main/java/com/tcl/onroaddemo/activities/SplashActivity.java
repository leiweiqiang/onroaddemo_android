package com.tcl.onroaddemo.activities;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import com.tcl.onroaddemo.R;

/**
 * Activity that installs required resources (from assets/MapResources.zip) to
 * the device
 */
public class SplashActivity extends Activity {

    /**
     * Path to the MapResources directory
     */
    public static String mapResourcesDirPath = "";

    private boolean update = false;

    private static final String TAG = "SplashActivity";
    public static int newMapVersionDetected = 0;

    /**
     * flag that shows whether the debug kit is enabled or not
     */
    private boolean debugKitEnabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initSplashView(((ViewGroup)findViewById(android.R.id.content)).getChildAt(0));
    }

    private void initSplashView(View view) {
        AlphaAnimation animation = new AlphaAnimation(0.3f, 1.0f);
        animation.setDuration(2000);
        view.startAnimation(animation);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                redirectToMainActivity();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    protected void redirectToMainActivity() {
        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }


}
