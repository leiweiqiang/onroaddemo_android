package com.tcl.onroaddemo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.baidu.mapapi.SDKInitializer;
import com.tcl.onroaddemo.R;
import com.tcl.onroaddemo.service.MapService;
import com.tcl.onroaddemo.widget.AddressFieldView;
import com.tcl.onroaddemo.widget.NavigateInfoView;
import com.tcl.onroaddemo.widget.SlideView;
import com.tcl.onroaddemo.widget.SpeechRecogizeView;

public class MainActivity extends AppCompatActivity implements
        SpeechRecogizeView.SpeechRecogizeViewListener, AddressFieldView.AddressFieldViewListener,
        NavigateInfoView.NavigateInfoViewListener {

    private View mapView;
    private AddressFieldView addressFieldView;
    private NavigateInfoView navigateInfoView;
    private SpeechRecogizeView speechRecogizeView;
    private LinearLayout slideViewInfLayout;
    private SlideView slideView;

    public enum NAVI_STATES{
        NAVI_IDLE,
        NAVI_SPEAKING,
        NAVI_RECOGNIZED,
        NAV_ROUTING
    }

    private void changeState(NAVI_STATES states){
        switch (states){
            case NAVI_IDLE:
                addressFieldView.setVisibility(View.GONE);
                navigateInfoView.setVisibility(View.GONE);
                slideViewInfLayout.setVisibility(View.GONE);
                speechRecogizeView.setVisibility(View.VISIBLE);
                break;

            case NAVI_SPEAKING:
                addressFieldView.setVisibility(View.GONE);
                navigateInfoView.setVisibility(View.GONE);
                slideViewInfLayout.setVisibility(View.GONE);
                speechRecogizeView.setVisibility(View.VISIBLE);
                break;

            case  NAVI_RECOGNIZED:
                addressFieldView.setVisibility(View.VISIBLE);
                navigateInfoView.setVisibility(View.GONE);
                slideViewInfLayout.setVisibility(View.VISIBLE);
                speechRecogizeView.setVisibility(View.GONE);
                break;

            case  NAV_ROUTING:
                addressFieldView.setVisibility(View.VISIBLE);
                navigateInfoView.setVisibility(View.GONE);
                slideViewInfLayout.setVisibility(View.VISIBLE);
                speechRecogizeView.setVisibility(View.GONE);
                break;

            default:
                break;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SDKInitializer.initialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        MapService.getDefaultService().init(this, null);

        FrameLayout mapViewHolder = (FrameLayout) findViewById(R.id.map_view_holder);
        mapView  = MapService.getDefaultService().getMapView();
        mapViewHolder.addView(mapView, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

        findViewById(R.id.center_me_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MapService.getDefaultService().centerMe();
            }
        });

        findViewById(R.id.route_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRouteBtnClick();
            }
        });


        LinearLayout addressFieldLayout = (LinearLayout)findViewById(R.id.address_field_view);
        addressFieldView = new AddressFieldView(getApplicationContext(), this);
        addressFieldLayout.addView(addressFieldView);

        LinearLayout navigateInfLayout = (LinearLayout)findViewById(R.id.navigate_info_view);
        navigateInfoView = new NavigateInfoView(getApplicationContext(), this);
        navigateInfLayout.addView(navigateInfoView);

        LinearLayout speechRecognizeInfLayout = (LinearLayout)findViewById(R.id.speech_recognize_view);
        speechRecogizeView = new SpeechRecogizeView(getApplicationContext(), this);
        speechRecognizeInfLayout.addView(speechRecogizeView);


        slideViewInfLayout = (LinearLayout)findViewById(R.id.slide_view);
        slideView = new SlideView(getApplicationContext());
        slideViewInfLayout.addView(slideView);

        changeState(NAVI_STATES.NAVI_IDLE);
    }

    @Override
    public void onPause() {
        MapService.getDefaultService().pause();
        super.onPause();
    }

    @Override
    public void onResume() {
        MapService.getDefaultService().resume();
        super.onResume();
    }


    @Override
    public void startSpeechRecognize() {

    }

    @Override
    public void finishSpeechRecognize() {
        changeState(NAVI_STATES.NAVI_RECOGNIZED);
    }

    @Override
    public void addressFieldBack() {
        changeState(NAVI_STATES.NAVI_IDLE);
    }

    @Override
    public void onRouteBtnClick() {
        Intent intent = new Intent(MainActivity.this, ConfigurationActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRouteTab1Click() {

    }

    @Override
    public void onRouteTab2Click() {

    }

}
