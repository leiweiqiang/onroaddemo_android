package com.tcl.onroaddemo.model;

public class MapCoordinate {
    private double longitude;
    private double latitude;

    public MapCoordinate() {
        latitude = 0.0D;
        longitude = 0.0D;
    }

    public MapCoordinate(double lat, double lon) {
        latitude = lat;
        longitude = lon;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double lon) {
        longitude = lon;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double lat) {
        latitude = lat;
    }

    public String toString() {
        return "[" + longitude + "," + latitude + "]";
    }
}

