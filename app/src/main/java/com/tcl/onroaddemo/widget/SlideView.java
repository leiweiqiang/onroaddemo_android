package com.tcl.onroaddemo.widget;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.tcl.onroaddemo.R;
import com.tcl.onroaddemo.widget.viewpagerindicator.CirclePageIndicator;

import org.json.JSONArray;

public class SlideView extends LinearLayout {
	private static final String TAG = "SlideView";

	private ViewPager viewPager;
	private PagerAdapter pagerAdapter;
	private CirclePageIndicator pageIndicator;
	private JSONArray dataArray;


	 public SlideView(Context context) {
	 super(context);

		LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflate.inflate(R.layout.slide_view, this);

		viewPager = (ViewPager) findViewById(R.id.viewpager);
		pagerAdapter = new SlidePagerAdapter();
		viewPager.setAdapter(pagerAdapter);

		pageIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
		pageIndicator.setViewPager(viewPager);
		pageIndicator.setRadius(4 * getResources().getDisplayMetrics().density);
		pageIndicator.setPageColor(0x88888888);
		pageIndicator.setFillColor(0x88FF0000);
		pageIndicator.setStrokeColor(0xFF888888);
		pageIndicator.setStrokeWidth(2);
	}

	public void setDataArray(JSONArray dataArray) {
		this.dataArray = dataArray;
		pagerAdapter.notifyDataSetChanged();
	}

	private class SlidePagerAdapter extends PagerAdapter {
		@Override
		public int getCount() {
//			return dataArray == null ? 0: dataArray.length();
			return 4;
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return arg0 == arg1;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((RouteCardView) object);
		}

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			RouteCardView routeCardView = new RouteCardView(getContext());
			container.addView(routeCardView);
			return routeCardView;
		}
	}

}
