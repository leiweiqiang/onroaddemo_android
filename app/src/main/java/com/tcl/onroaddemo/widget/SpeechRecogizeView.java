package com.tcl.onroaddemo.widget;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tcl.onroaddemo.R;

public class SpeechRecogizeView extends LinearLayout {

    private UIHandler uiHandler;
    private TextView micInfoText;

    public interface SpeechRecogizeViewListener{
        public void startSpeechRecognize();
        public void finishSpeechRecognize();
    }

    private SpeechRecogizeViewListener listener;

    private class UIHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            listener.finishSpeechRecognize();
            micInfoText.setText("Tap to speak");
            super.handleMessage(msg);
        }
    }

    public SpeechRecogizeView(Context context, final SpeechRecogizeViewListener listener) {
        super(context);
        this.listener = listener;
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.speech_recognize_view, this);
        setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));

        micInfoText = (TextView) findViewById(R.id.mic_info_text);

        uiHandler = new UIHandler();

        findViewById(R.id.micphone_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listener.startSpeechRecognize();
                micInfoText.setText("Recognizing...");

                uiHandler.sendEmptyMessageDelayed(0x567, 3000);
            }
        });

    }
}
