package com.tcl.onroaddemo.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.tcl.onroaddemo.R;

public class AddressFieldView extends LinearLayout {

    private AddressFieldViewListener mlistener;

    public interface AddressFieldViewListener{
        public void addressFieldBack();
    }

    public AddressFieldView(Context context, AddressFieldViewListener listener) {
        super(context);
        this.mlistener = listener;
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.address_field_view, this);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        findViewById(R.id.btn_back).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mlistener.addressFieldBack();
            }
        });
    }
}
