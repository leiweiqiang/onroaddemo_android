package com.tcl.onroaddemo.widget;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.tcl.onroaddemo.R;

public class NavigateInfoView extends LinearLayout {

    private NavigateInfoViewListener mlistener;
    private LinearLayout route1, route2;

    public interface NavigateInfoViewListener{
        public void onRouteBtnClick();
        public void onRouteTab1Click();
        public void onRouteTab2Click();
    }

    public NavigateInfoView(Context context, NavigateInfoViewListener listener) {
        super(context);
        mlistener = listener;
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.navigate_info_view, this);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));

        findViewById(R.id.btn_route).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mlistener.onRouteBtnClick();
            }
        });

        route1 = (LinearLayout)findViewById(R.id.tab_route1);
        route1.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                route1.setBackgroundColor(Color.parseColor("#DDDDDD"));
                route2.setBackgroundColor(Color.parseColor("#FFFFFF"));
                mlistener.onRouteTab1Click();
            }
        });

        route2 = (LinearLayout)findViewById(R.id.tab_route2);
        route2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                route2.setBackgroundColor(Color.parseColor("#DDDDDD"));
                route1.setBackgroundColor(Color.parseColor("#FFFFFF"));
                mlistener.onRouteTab2Click();
            }
        });
    }
}
