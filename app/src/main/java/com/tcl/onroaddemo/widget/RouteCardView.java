package com.tcl.onroaddemo.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.tcl.onroaddemo.R;

public class RouteCardView extends LinearLayout {


    public RouteCardView(Context context) {
        super(context);
        LayoutInflater inflate = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflate.inflate(R.layout.route_card_view, this);
        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
    }
}
