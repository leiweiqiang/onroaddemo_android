package com.tcl.onroaddemo.service.baidu;

import android.content.Context;
import android.location.LocationManager;
import android.view.View;

import com.baidu.mapapi.map.BaiduMapOptions;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapView;
import com.tcl.onroaddemo.model.MapCoordinate;
import com.tcl.onroaddemo.service.MapService;

public class BaiduMapService extends MapService {

    public MapView mMapView = null;

    private  LocationManager manager;

    public BaiduMapService() {
    }


    @Override
    public void init(Context context, Listener listener) {
//        SDKInitializer.initialize(context);
        // 初始化地图
        BaiduMapOptions mapOptions = new BaiduMapOptions();
        mapOptions.scaleControlEnabled(false); // 隐藏比例尺控件
        mapOptions.zoomControlsEnabled(false); // 隐藏缩放控件
        mapOptions.mapStatus(new MapStatus.Builder().zoom(18).build()); // 设定缩放级别。

        mMapView = new MapView(context, mapOptions);
        mMapView.setClickable(true);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void centerMe() {

    }

    @Override
    public void centerLocation(MapCoordinate location) {

    }

    @Override
    public View getMapView() {
        return mMapView;
    }

    @Override
    public MapCoordinate getCurrentLocation() {
        return null;
    }

    @Override
    public void addAnnotation(int type, MapCoordinate position) {

    }

    @Override
    public void addAnnotation(int type, MapCoordinate position, View view) {

    }

    @Override
    public void deleteAnnotation(int type) {

    }
}
