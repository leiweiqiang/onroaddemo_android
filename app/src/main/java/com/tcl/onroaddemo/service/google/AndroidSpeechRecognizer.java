package com.tcl.onroaddemo.service.google;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognitionService;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;

import com.tcl.onroaddemo.onroad.OnRoadApplication;
import com.tcl.onroaddemo.service.SpeechRecognizerService;
import com.tcl.onroaddemo.utils.SystemUtil;

import java.util.ArrayList;
import java.util.List;

public class AndroidSpeechRecognizer extends SpeechRecognizerService {
    private static final String TAG = "AndroidSpeechRecognizer";
    private static final long DEFAULT_RECOGNIZE_TIMEOUT = 30000;
    
    private SpeechRecognizer speechRecognizer;
    private boolean isRecognizing;
    private AudioManager audioManager;
    private int musicVolume;
    private long recognizeTimeout;
    private long recognizeStartTime;

    private Context context;

    @Override
    public void init(Context context) {
        this.context = context;
        ComponentName cn = null;
        final List<ResolveInfo> list = context.getPackageManager().queryIntentServices(
            new Intent(RecognitionService.SERVICE_INTERFACE), 0);
        for (ResolveInfo ri : list) {
            cn = new ComponentName(ri.serviceInfo.packageName, ri.serviceInfo.name);
            break;
        }
        if (cn == null) {
            // TODO: throw exception
            return;
        }

//        Log.d(TAG , "init AndroidSpeechRecognizer");
        speechRecognizer = SpeechRecognizer.createSpeechRecognizer(context, cn);
        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        musicVolume = -1;
        recognizeTimeout = DEFAULT_RECOGNIZE_TIMEOUT;
        recognizeStartTime = -1;
    }

    private class RetryAction implements Runnable {
        SpeechRecognizerService.Listener listener;
        int retryCount;

        @Override
        public void run() {
            ++retryCount;
            startRecognizeInternal(listener);
        }
    }
    private RetryAction retryAction;

    private class CancelAction implements Runnable {
        SpeechRecognizerService.Listener listener;

        @Override
        public void run() {
            cancelAction = null;
            cancelRecognize();
            listener.onError(SpeechRecognizerService.ERROR_SPEECH_TIMEOUT);
        }
    }
    private CancelAction cancelAction;

    @Override
    public void startRecognize(SpeechRecognizerService.Listener listener) {
        startRecognize(DEFAULT_RECOGNIZE_TIMEOUT, listener);
    }

    @Override
    public void startRecognize(long timeout, Listener listener) {
        if (listener == null) {
            // TODO: throw error
            return;
        }
        Log.d(TAG , "AndroidSpeechRecognizer:startRecognize");
        cancelRecognize();
        isRecognizing = true;
        recognizeTimeout = timeout;
        recognizeStartTime = -1;
        musicVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        Log.d(TAG , "save current music volume: " + musicVolume);
        startRecognizeInternal(listener);
    }

    private void startRecognizeInternal(final SpeechRecognizerService.Listener listener) {
        if (SystemUtil.isMicrophoneReady()) {
            Log.d(TAG , "AndroidSpeechRecognizer:startRecognize started");
            speechRecognizer.setRecognitionListener(new RecognitionListener() {
                @Override
                public void onReadyForSpeech(Bundle params) {
                    Log.d(TAG , "onReadyForSpeech");
                    retryAction = null;
                    if (recognizeStartTime < 0) {
                        recognizeStartTime = System.currentTimeMillis();
                        audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0);
                    }
                    if (cancelAction == null) {
                        cancelAction = new CancelAction();
                        cancelAction.listener = listener;
                        OnRoadApplication.getInstance().postToUiThread(cancelAction, recognizeTimeout);
                    }
                    listener.onReadyToSpeech();
                }

                @Override
                public void onBeginningOfSpeech() {
                    Log.d(TAG , "onBeginningOfSpeech");
                    if (cancelAction != null) {
                        OnRoadApplication.getInstance().cancelAction(cancelAction);
                        cancelAction = null;
                    }
                    listener.onBeginningOfSpeech();
                }

                @Override
                public void onRmsChanged(float rmsdB) {
                }

                @Override
                public void onBufferReceived(byte[] buffer) {
                }

                @Override
                public void onEndOfSpeech() {
                    Log.d(TAG , "onEndOfSpeech");
                    listener.onEndOfSpeech();
                }

                @Override
                public void onError(int error) {
                    Log.d(TAG , "onError: " + error);
                    if (error == SpeechRecognizer.ERROR_NETWORK) {
                        if (retryAction == null) {
                            retryAction = new RetryAction();
                            retryAction.listener = listener;
                            retryAction.retryCount = 0;
                        }
                        if (retryAction.retryCount < 20) {
                            OnRoadApplication.getInstance().postToUiThread(retryAction, 100);
                            return;
                        }
                    }

                    if (error == SpeechRecognizer.ERROR_SPEECH_TIMEOUT &&
                        System.currentTimeMillis() - recognizeStartTime < recognizeTimeout) {
                        startRecognizeInternal(listener);
                    } else {
                        endRecognizeInternal();
                        if (error == SpeechRecognizer.ERROR_SPEECH_TIMEOUT) {
                            listener.onError(SpeechRecognizerService.ERROR_SPEECH_TIMEOUT);
                        } else {
                            listener.onError(SpeechRecognizerService.ERROR_OTHER);
                        }
                    }
                }

                @Override
                public void onResults(Bundle results) {
                    Log.d(TAG , "onResults");
                    endRecognizeInternal();
                    ArrayList<String> data = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                    if (data.size() > 0) {
                        Log.d(TAG , data.get(0));
                        listener.onResult(data.get(0));
                    } else {
                        listener.onResult("");
                    }
                }

                @Override
                public void onPartialResults(Bundle partialResults) {
                    Log.d(TAG , "onPartialResults");
                    ArrayList<String> data = partialResults.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                    if (data.size() > 0) {
                        listener.onPartialResult(data.get(0));
                    }
                }

                @Override
                public void onEvent(int eventType, Bundle params) {

                }
            });
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
            intent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
            intent.putExtra(RecognizerIntent.EXTRA_PREFER_OFFLINE, true);
            speechRecognizer.startListening(intent);
        } else {
            if (retryAction == null) {
                retryAction = new RetryAction();
                retryAction.listener = listener;
                retryAction.retryCount = 0;
            }
            if (retryAction.retryCount < 20) {
                OnRoadApplication.getInstance().postToUiThread(retryAction, 100);
            } else {
                isRecognizing = false;
                listener.onError(SpeechRecognizerService.ERROR_OTHER);
            }
        }
    }

    @Override
    public void stopRecognize() {
        if (isRecognizing && speechRecognizer != null) {
            speechRecognizer.stopListening();
        }
        endRecognizeInternal();
    }

    @Override
    public void cancelRecognize() {
        if (isRecognizing && speechRecognizer != null) {
            speechRecognizer.cancel();
        }
        endRecognizeInternal();
    }

    private void endRecognizeInternal() {
        if (retryAction != null) {
            OnRoadApplication.getInstance().cancelAction(retryAction);
            retryAction = null;
        }
        if (cancelAction != null) {
            OnRoadApplication.getInstance().cancelAction(cancelAction);
            cancelAction = null;
        }
        if (musicVolume >= 0) {
            Log.d(TAG , "set music volume back to " + musicVolume);
            audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, musicVolume, 0);
            musicVolume = -1;
        }
        isRecognizing = false;
    }

    @Override
    public boolean isRecognizing() {
        return isRecognizing;
    }

    @Override
    public void destroy() {
        if (speechRecognizer != null) {
            speechRecognizer.destroy();
            speechRecognizer = null;
        }
    }
}
