package com.tcl.onroaddemo.service;

import android.content.Context;
import android.speech.SpeechRecognizer;

import com.tcl.onroaddemo.service.google.AndroidSpeechRecognizer;


public abstract class SpeechRecognizerService {
    public static final int ERROR_SPEECH_TIMEOUT = 1;
    public static final int ERROR_OTHER = 5;

    public interface Listener {
        public void onReadyToSpeech();
        public void onBeginningOfSpeech();
        public void onEndOfSpeech();
        public void onResult(String result);
        public void onPartialResult(String partialResult);
        public void onError(int error);
    }

    public static abstract class AbstractListener implements Listener {
        public void onReadyToSpeech() {}
        public void onBeginningOfSpeech() {}
        public void onEndOfSpeech() {}
        public void onPartialResult(String partialResult) {}
        public void onError(int error) {}
    }

    public abstract void init(Context context);
    public abstract void startRecognize(Listener listener);
    public abstract void startRecognize(long timeout, Listener listener);
    public abstract void stopRecognize();
    public abstract void cancelRecognize();
    public abstract boolean isRecognizing();
    public abstract void destroy();

    private static SpeechRecognizerService sDefaultService;

    public static SpeechRecognizerService getDefaultService(Context context) {
        if (sDefaultService == null) {
            if (SpeechRecognizer.isRecognitionAvailable(context)) {
                sDefaultService = new AndroidSpeechRecognizer();
            } else {
//                sDefaultService = new NuanceCloudRecognizer();
            }
        }
        return sDefaultService;
    }
}
