package com.tcl.onroaddemo.service;

import android.content.Context;

import com.tcl.onroaddemo.service.google.AndroidTtsService;


public abstract class TtsService {
    public static final int SPEECH_PRIORITY_LOW = 1;
    public static final int SPEECH_PRIORITY_NORMAL = 2;
    public static final int SPEECH_PRIORITY_HIGH = 3;

    public static interface Listener {
        public void onFinished();
        public void onStopped();
        public void onError();
    }

    public abstract void init(Context context);
    public abstract void speak(String text, String source, int priority, TtsService.Listener listener);
    public abstract void stop(String source);
    public abstract void destroy();

    private static TtsService sDefaultService;

    public static TtsService getDefaultService() {
        if (sDefaultService == null) {
            sDefaultService = new AndroidTtsService();
        }
        return sDefaultService;
    }
}
