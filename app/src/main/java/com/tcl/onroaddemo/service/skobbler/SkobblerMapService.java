package com.tcl.onroaddemo.service.skobbler;

import android.content.Context;
import android.view.View;

import com.tcl.onroaddemo.model.MapCoordinate;
import com.tcl.onroaddemo.service.MapService;

public class SkobblerMapService extends MapService {
    public SkobblerMapService() {
    }

    @Override
    public void init(Context context, Listener listener) {
        // TODO: 15/12/29 add skobbler map
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void centerMe() {

    }

    @Override
    public void centerLocation(MapCoordinate location) {

    }

    @Override
    public View getMapView() {
        return null;
    }

    @Override
    public MapCoordinate getCurrentLocation() {
        return null;
    }

    @Override
    public void addAnnotation(int type, MapCoordinate position) {

    }

    @Override
    public void addAnnotation(int type, MapCoordinate position, View view) {

    }

    @Override
    public void deleteAnnotation(int type) {

    }
}
