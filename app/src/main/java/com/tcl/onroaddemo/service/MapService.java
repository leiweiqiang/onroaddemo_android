package com.tcl.onroaddemo.service;


import android.content.Context;
import android.view.View;

import com.tcl.onroaddemo.model.MapCoordinate;
import com.tcl.onroaddemo.service.baidu.BaiduMapService;

public abstract class MapService {
    public static final int ANNOTATION_PIN = 1;
    public static final int ANNOTATION_START_POINT = 2;
    public static final int ANNOTATION_DESTINATION_POINT = 3;
    public static final int ANNOTATION_DESTINATION_FLAG = 4;
    public static final int ANNOTATION_MY_CAR = 5;

    public static interface Listener {
        public void onServiceReady();
    }

    public abstract void init(Context context, MapService.Listener listener);
    public abstract void pause();
    public abstract void resume();
    public abstract void destroy();
    public abstract void centerMe();
    public abstract void centerLocation(MapCoordinate location);
    public abstract View getMapView();
    public abstract MapCoordinate getCurrentLocation();
    public abstract void addAnnotation(int type, MapCoordinate position);
    public abstract void addAnnotation(int type, MapCoordinate position, View view);
    public abstract void deleteAnnotation(int type);

    private static MapService sDefaultService;
    public static MapService getDefaultService() {
        if (sDefaultService == null) {
//            sDefaultService = new SkobblerMapService();
            sDefaultService = new BaiduMapService();
        }
        return sDefaultService;
    }
}
