package com.tcl.onroaddemo.service.google;

import android.content.Context;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;

import com.tcl.onroaddemo.onroad.OnRoadApplication;
import com.tcl.onroaddemo.service.TtsService;

import java.util.HashMap;
import java.util.Locale;

public class AndroidTtsService extends TtsService {
    private static final int SPEECH_TYPE_PROMPTY = 1;
    private static final int SPEECH_TYPE_INFO = 1;

    private static class UtteranceInfo {
        TtsService.Listener listener;
        boolean isStopped;
    }

    private TextToSpeech speechEngine;
    private boolean isReady;
    private int speechId;
    private HashMap<String, String> params;
    private HashMap<String, UtteranceInfo> utteranceInfos;
    private int lastSpeechPriority;
    private String lastSpeechSource;
    private String currentUtteranceId;

    @Override
    public void init(Context context) {
        isReady = false;
        speechEngine = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = speechEngine.setLanguage(Locale.ENGLISH);
                    if (result != TextToSpeech.LANG_MISSING_DATA && result != TextToSpeech.LANG_NOT_SUPPORTED) {
                        isReady = true;
                    }
                }
            }
        });
        speechEngine.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
                currentUtteranceId = utteranceId;
            }

            @Override
            public void onDone(final String utteranceId) {
                currentUtteranceId = null;
                OnRoadApplication.getInstance().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        UtteranceInfo utteranceInfo = utteranceInfos.get(utteranceId);
                        if (utteranceInfo != null) {
                            if (utteranceInfo.isStopped) {
                                utteranceInfo.listener.onStopped();
                            } else {
                                utteranceInfo.listener.onFinished();
                            }
                            utteranceInfos.remove(utteranceId);
                        }
                    }
                });
            }

            @Override
            public void onError(final String utteranceId) {
                currentUtteranceId = null;
                OnRoadApplication.getInstance().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        UtteranceInfo utteranceInfo = utteranceInfos.get(utteranceId);
                        if (utteranceInfo != null) {
                            utteranceInfo.listener.onError();
                            utteranceInfos.remove(utteranceId);
                        }
                    }
                });
            }
        });
        params = new HashMap<>();
        utteranceInfos = new HashMap<>();
        speechId = 0;
        lastSpeechPriority = 0;
        lastSpeechSource = "";
        currentUtteranceId = null;
    }

    @Override
    public void speak(String text, String source, int priority, TtsService.Listener listener) {
        if (!isReady || (speechEngine.isSpeaking() && priority < lastSpeechPriority)) {
            return;
        }
        lastSpeechSource = source;
        lastSpeechPriority = priority;
        String utteranceId = String.valueOf(++speechId);
        params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, utteranceId);
        if (listener != null) {
            UtteranceInfo utteranceInfo = new UtteranceInfo();
            utteranceInfo.listener = listener;
            utteranceInfo.isStopped = false;
            utteranceInfos.put(utteranceId, utteranceInfo);
        }
        if (currentUtteranceId != null) {
            UtteranceInfo utteranceInfo = utteranceInfos.get(currentUtteranceId);
            if (utteranceInfo != null) {
                utteranceInfo.isStopped = true;
            }
        }
        speechEngine.speak(text, TextToSpeech.QUEUE_FLUSH, params);

    }

    @Override
    public void stop(String source) {
        if (!isReady) {
            return;
        }
        if (source == null || source.isEmpty() || source.equals(lastSpeechSource)) {
            if (currentUtteranceId != null) {
                UtteranceInfo utteranceInfo = utteranceInfos.get(currentUtteranceId);
                if (utteranceInfo != null) {
                    utteranceInfo.isStopped = true;
                }
            }
            speechEngine.stop();
        }
    }

    @Override
    public void destroy() {
        speechEngine.stop();
        speechEngine.shutdown();
    }
}
