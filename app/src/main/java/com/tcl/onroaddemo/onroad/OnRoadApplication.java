package com.tcl.onroaddemo.onroad;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.StringRes;

import com.tcl.onroaddemo.service.SpeechRecognizerService;
import com.tcl.onroaddemo.service.TtsService;

public class OnRoadApplication extends Application{
    private static final String PREFS_NAME = "OnRoad";

    private static OnRoadApplication sInstance;

    private Thread uiThread;
    private Handler handler;

    private SharedPreferences prefs;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        uiThread = Thread.currentThread();
        handler = new Handler();
        prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public static OnRoadApplication getInstance() {
        return sInstance;
    }

    public final void runOnUiThread(Runnable action) {
        if (Thread.currentThread() != uiThread) {
            handler.post(action);
        } else {
            action.run();
        }
    }

    public final void postToUiThread(Runnable action) {
        if (action == null) {
            return;
        }
        handler.post(action);
    }

    public final void postToUiThread(Runnable action, long delay) {
        if (action == null) {
            return;
        }
        handler.postDelayed(action, delay);
    }

    public final void cancelAction(Runnable action) {
        if (action == null) {
            return;
        }
        handler.removeCallbacks(action);
    }

    public void startSpeechRecognize(SpeechRecognizerService.Listener listener) {
        TtsService.getDefaultService().stop(null);
        SpeechRecognizerService.getDefaultService(getApplicationContext()).startRecognize(listener);
    }

    public void startSpeechRecognize(long timeout, SpeechRecognizerService.Listener listener) {
        TtsService.getDefaultService().stop(null);
        SpeechRecognizerService.getDefaultService(getApplicationContext()).startRecognize(timeout, listener);
    }

    public void tts(String text, String source, int priority, TtsService.Listener listener) {
        if (SpeechRecognizerService.getDefaultService(getApplicationContext()).isRecognizing()) {
            return;
        }
        TtsService.getDefaultService().speak(text, source, priority, listener);
    }

    public void tts(String text, String source, int priority) {
        tts(text, source, priority, null);
    }

    public void tts(String text, String source) {
        tts(text, source, TtsService.SPEECH_PRIORITY_NORMAL, null);
    }

    public void tts(@StringRes int resourceId, String source, int priority, TtsService.Listener listener) {
        tts(getString(resourceId), source, priority, listener);
    }

    public void tts(@StringRes int resourceId, String source, int priority) {
        tts(getString(resourceId), source, priority, null);
    }

    public void tts(@StringRes int resourceId, String source) {
        tts(getString(resourceId), source, TtsService.SPEECH_PRIORITY_NORMAL, null);
    }

    public void tts(@StringRes int resourceId) {
        tts(getString(resourceId), "", TtsService.SPEECH_PRIORITY_NORMAL, null);
    }

    public void savePreference(String key, int value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public int getIntPreference(String key, int defValue) {
        return prefs.getInt(key, defValue);
    }

    public void savePreference(String key, String value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getStringPreference(String key, String defValue) {
        return prefs.getString(key, defValue);
    }

}
